#include <stdio.h>
#include <stdint.h>

uint32_t order(char *buf){
    char r[4];
    
    r[0] = buf[3];r[1] = buf[2];r[2] = buf[1];r[3] = buf[0];
    *(uint32_t *)buf = *(uint32_t *)r;
    
    return 0;
};

int main(int argc, char* argv[])
{
    uint32_t buf1 = 0;
    uint32_t buf2 = 0;

    if(argc==1){
        printf("usage : add-nbo <file1> <file2>\n");
        return 0;
    }

    FILE* fp1 = fopen(argv[1], "r");
    FILE* fp2 = fopen(argv[2], "r");

    fread(&buf1, 1, 4, fp1);
    fread(&buf2, 1, 4, fp2);

    fclose(fp1);
    fclose(fp2);

    order((char *)&buf1);
    order((char *)&buf2);

    printf("%1$u(0x%1$x) + %2$u(0x%2$x) = %3$u(0x%3$x)\n", buf1, buf2, buf1+buf2);

    return 0;
}